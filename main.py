# encoding: utf-8

import sys
import os
import json
from threading import Thread
from server import Servidor
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import QUrl,QThread
from PyQt4.QtGui import QApplication
from PyQt4.QtCore import QMetaObject, QUrl
from PyQt4.QtGui import QVBoxLayout, QMainWindow, QWidget
from PyQt4.QtWebKit import QWebView



with open("src/config.json","r") as f:
	datos = json.load(f)

PORT = datos["puerto"]
ROOT_URL = 'http://localhost:{}'.format(PORT)

class window(QWebView):
	def __init__(self):
		self.titulo = datos["titulo"]
		self.ancho = datos["ancho"]
		self.alto = datos["alto"]
		QMainWindow.__init__(self)
		self.setWindowTitle(self.titulo)
		self.resize(self.ancho, self.alto)
		self.setWindowIcon(QtGui.QIcon('src/assets/logo.png'))
		self.file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "src/index.html"))
		self.local_url = QUrl.fromLocalFile(self.file_path)
		QMetaObject.connectSlotsByName(self)
	def run(self):
		self.load(QUrl("http://localhost:5312"))
		self.show()
		

if __name__ == "__main__":
	app = QApplication(sys.argv)
	w = window()
	s = Servidor()
	tw = Thread(target= w.run)
	ts = Thread(target= s.run, daemon=True)
	ts.start()
	w.run()
	sys.exit(app.exec_())
	
	
