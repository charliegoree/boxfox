import os
import json
from flask import Flask



with open("src/config.json","r") as f:
	datos = json.load(f)

PORT = datos["puerto"]
ROOT_URL = 'http://localhost:{}'.format(PORT)
root = os.path.join(os.path.dirname(os.path.abspath(__file__)), "src")
class Servidor():
	app = None
	saludo = "hola mundo"
	def __init__(self):
		self.app = Flask(__name__,static_url_path='')
		@self.app.route('/')
		def home():
			data = open('src/index.html').read()
			return data
	def run(self):
		self.app.run(port=PORT)
		
		
